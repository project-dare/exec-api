Execution API
=============

.. toctree::
        :caption: Execution API

.. automodule:: app.run
   :members: