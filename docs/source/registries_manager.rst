CWL & dispel4py registries manager
==================================

.. toctree::
        d4p_registry_manager
        cwl_registry_manager

DARE platform supports workflow execution in dispel4py and CWL. For this purpose, it includes two kind of registries.
In this section, we provide technical documentation for the client-side library regarding the two registries.