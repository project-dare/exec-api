from os import environ

login_host = environ.get("DARE_LOGIN_PUBLIC_SERVICE_HOST", "localhost")
login_port = environ.get("DARE_LOGIN_PUBLIC_SERVICE_PORT", "80")
LOGIN_URL = "http://{}:{}".format(login_host, login_port) if login_host != "localhost" else \
    "https://platform.dare.scai.fraunhofer.de/dare-login"

cwl_host = environ.get("WORKFLOW_REGISTRY_PUBLIC_SERVICE_HOST", "localhost")
cwl_port = environ.get("WORKFLOW_REGISTRY_PUBLIC_SERVICE_PORT", "8000")
CWL_URL = "http://{}:{}".format(cwl_host, cwl_port) if cwl_host != "localhost" else \
    "https://platform.dare.scai.fraunhofer.de/workflow-registry"
