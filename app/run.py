import json
import logging
from logging.config import dictConfig
from os.path import join, exists

import requests
import yaml
from flask import Flask, request, send_file, g
from kubernetes.config.config_exception import ConfigException

try:
    from job_handler import D4pJobPreparator, CwlJobPreparator, JobPreparator, my_pods, init_from_yaml
    from file_handler import upload_file, b2drop_upload, check_for_sub_dir, cleanup_folder, list_user_folders, \
        list_user_files, create_missing_folders_for_user
    from settings import LOGIN_URL
except (BaseException, Exception):
    # Used for the sphinx build
    from app.settings import LOGIN_URL
    from app.job_handler import D4pJobPreparator, CwlJobPreparator, JobPreparator, my_pods, init_from_yaml
    from app.file_handler import upload_file, b2drop_upload, check_for_sub_dir, cleanup_folder, list_user_folders, \
        list_user_files, create_missing_folders_for_user

dictConfig({
    'version': 1,
    'formatters': {
        'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
        }
    },
    'handlers': {
        'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default'
        }
    },
    'root': {
        'level': 'DEBUG',
        'handlers': ['wsgi']
    },
})

app = Flask(__name__)
logger = app.logger

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

# Get namespace from yaml
try:
    try:
        r = requests.get('https://gitlab.com/project-dare/dare-platform/tree/master/k8s/namespaced/exec-api-dp.yaml')
        name_space = yaml.safe_load(r.text)['metadata']['namespace']
        # Current pod running flask api
        host_pod = init_from_yaml(name_space)
        # Namespace found in yaml, but not in kubernetes
        if host_pod is None:
            name_space = 'default'
            # Current pod running flask api
            host_pod = init_from_yaml(name_space)
    # No namespace in yaml
    except (ConfigException, Exception):
        name_space = 'default'
        # Current pod running flask api
        host_pod = init_from_yaml(name_space)

    # D4P OPENMPI
    _d4p = {"jobname": "d4p-openmpi", "mountname": host_pod.spec.containers[0].volume_mounts[0].name,
            "mountpath": host_pod.spec.containers[0].volume_mounts[0].mount_path,
            "volname": host_pod.spec.volumes[0].name,
            "fsname": host_pod.spec.volumes[0].flex_volume.options["fsName"]}
    _code = _d4p["mountpath"] + "/code.py"
except (ConfigException, Exception):
    # D4P OPENMPI
    _d4p = {"jobname": "d4p-openmpi", "mountname": "d4p-openmpi-store",
            "mountpath": "/home/mpiuser/sfs", "volname": "d4p-openmpi-store", "fsname": "mysfs"}
    _code = _d4p["mountpath"] + "/code.py"


def exclude_from_auth(func):
    func.exclude_from_auth = True
    return func


@app.route('/create-folders', methods=['POST'])
@exclude_from_auth
def create_user_folders():
    """
    First method after authentication, to be used by a new user. Creates its workspace in the Shared File System.
    Each user has its own directory which will contain three subdirectories: i.e. uploads, runs and debug, the latter
    is used for testing purposes.

    E.g. the base directory of a user with username = some_username will be:
        "/home/mpiuser/sfs/some_username". Inside the base directory there are three folders:
            1. uploads: folder to store all the user's uploaded files
            2. runs: folder to store all the executions of the user
            3. debug: folder to store all the test executions of the user
    """
    data = {x: y for x, y in request.form.items()}
    username = data["username"]
    print("Mount path: {}".format(_d4p['mountpath']))
    print("Username: {}".format(username))
    try:
        create_missing_folders_for_user(_d4p['mountpath'], username)
        return json.dumps({'result': 'success'}), 200
    except(OSError, Exception):
        return json.dumps({'result': 'failed'}), 500


# Start d4p "workflow"
@app.route('/run-d4p', methods=['POST'])
def run_d4p():
    """
    API endpoint for dispel4py workflow execution. The Execution API spawns additional containers (specified by
    the user), retrieves the relevant code from the D4p Information Registry and writes the final outputs and logs
    in the respective execution directory.
    """
    # Load request data
    data = {x: y for x, y in request.form.items()}
    data["username"] = g.username
    data["user_id"] = g.user_id
    data["issuer"] = g.issuer

    try:
        d4p_job_preparator = D4pJobPreparator(d4p_settings=_d4p, namespace=name_space, user_id=g.user_id,
                                              username=g.username, mountpath=_d4p["mountpath"],
                                              pod_prefix=_d4p["jobname"], pod_suffix=data.get("additional_name", None),
                                              token=data["access_token"], issuer=g.issuer, nodes=data["n_nodes"],
                                              workflow_name=data["name"], workflow_id=data["impl_id"],
                                              workspace_id=data["wrkspce_id"], logger=logger,
                                              d4p_args=data["d4p_args"], reqs=data["reqs"],
                                              image_name=data.get("image", None),
                                              package_name=data["pckg"])
        if d4p_job_preparator.found_workflow:
            response = d4p_job_preparator.create_job()
            return response
        else:
            logger.error("Workflow not found!")
            return "Workflow not found", 500
    except (Exception, BaseException) as e:
        logger.error("An error occurred while trying to spawn containers {}".format(e))


@app.route("/run-cwl", methods=["POST"])
def run_cwl():
    """
    CWL execution context. Retrieves already registered workflows, along with their execution environment
    and executes the job
    """
    # Load request data
    try:
        data = {x: y for x, y in request.form.items()}
        # validate token and get user data
        data["username"] = g.username
        data["user_id"] = g.user_id
        data["issuer"] = g.issuer
        save_workflow = data.get("save_workflow", None)
        cwl_job_preparator = CwlJobPreparator(d4p_settings=_d4p, namespace=name_space, user_id=g.user_id,
                                              username=g.username, mountpath=_d4p["mountpath"], pod_prefix="cwl",
                                              pod_suffix=data.get("additional_name", None), token=data["access_token"],
                                              issuer=g.issuer, nodes=data["nodes"], workflow_name=data["workflow_name"],
                                              workflow_version=data["workflow_version"], save_workflow=save_workflow,
                                              input_data=data.get("input_data", "None"), logger=logger)
        if cwl_job_preparator.found_workflow:
            response = cwl_job_preparator.create_job()
            return response
        else:
            logger.error("Workflow not found!")
            return "Workflow not found!", 500
    except (BaseException, Exception) as e:
        msg = "An error occurred in run-cwl: {}".format(e)
        logger.error(msg)
        return msg, 500


# Upload file in exec-api filesystem
@app.route("/upload", methods=['POST'])
def upload():
    """
    API endpoint providing the functionality to upload files in the DARE platform.
    All files are saved under the user's directory: ../username/uploads/. The users can provide a folder and a file
    name. If they provide a folder name the file will be stored under ../username/uploads/<folder name>/<file name>,
    otherwise the file will be saved directly in the uploads folder of the user.
    """
    # Load request data
    dataset_name = request.args.get('dataset_name')
    path = request.args.get('path')
    access_token = request.args.get('access_token')

    data = {"dataset_name": dataset_name, "access_token": access_token, "path": path, "username": g.username,
            "user_id": g.user_id}
    # args
    file = request.files['file']
    # Check for missing arguments
    return upload_file(data, file, _d4p["mountpath"])


# List directories

@app.route("/my-files", methods=['GET'])
def my_files():
    """
    API endpoint to list your directories in the platform. It shows the content of the three main
    directories of the user, i.e. uploads, runs, debug
    """
    access_token = request.args.get("access_token")
    data = {"access_token": access_token, "username": g.username, "user_id": g.user_id}
    result = list_user_folders(data=data, mountpath=_d4p["mountpath"])
    return json.dumps(result)


# List individual files
@app.route("/list", methods=['GET'])
def list_files():
    """
    API endpoint to list your files inside a specific directory in the DARE platform. Use first the my_files endpoint,
    to list all your directories inside your 3 main folders and then choose a specific path to list the files
    inside it
    """
    path = request.args.get('path')
    ret = list_user_files(path)
    return json.dumps(ret)


# Download file from path
@app.route("/download", methods=['GET'])
def download():
    """
    API endpoint to download a file from the DARE platform. The users should provide the full path to the selected
    file in order to download it locally.
    """
    path = request.args.get('path')
    try:
        return send_file(path, attachment_filename=path.split('/')[-1])
    except (FileNotFoundError, Exception) as e:
        return "File not found! {}".format(e), 404


@app.route("/file-content", methods=["GET"])
def json_file_content():
    run_dir = request.args.get("run_dir")
    out_dir = request.args.get("output_dir")
    if not out_dir:
        out_dir = "output"
    path = request.args.get("path")
    filename = request.args.get("filename")
    username = g.username
    try:
        path = "/home/mpiuser/sfs/{}/runs/{}/{}".format(username, run_dir, out_dir) if run_dir else path
        file_path = join(path, filename)
        logger.debug("Looking for path: {}".format(file_path))
        if not exists(file_path):
            logger.debug("Path does not exist!")
            return "File is not created yet", 200
        with open(file_path, "r") as f:
            content = json.loads(f.read())
        return json.dumps(content)
    except (FileNotFoundError, Exception) as e:
        msg = "An error occurred while trying to read file with name {}! {}".format(filename, e)
        return msg, 500


@app.route("/send2drop", methods=['POST'])
def send2drop():
    """
    API endpoint to upload a file in B2DROP. The environmental variables B2DROP_USER and B2DROP_PWD should have
    been configured properly in the exec-api deployment yaml file in order to have acces to B2DROP
    """
    data = {x: y for x, y in request.form.items()}
    link = b2drop_upload(data, logger=logger)
    return link


@app.route("/my-pods", methods=['GET'])
def my_runs():
    """
    API endpoint to list the containers related to a MPI job
    """
    return json.dumps(my_pods(g.username, name_space))


@app.route("/cleanup", methods=["GET"])
def clean_up_folders():
    """
    API endpoint to cleanup the user space in the DARE platform. Users should determine which directories to be cleaned,
    e.g. the uploads, runs etc.
    """
    username = g.username
    folders_to_clean = []
    runs = request.args.get("runs", None)
    uploads = request.args.get("uploads", None)
    subfolder_uploads = request.args.get("subfolder_uploads", None)
    subfolder_runs = request.args.get("subfolder_runs", None)
    try:
        if runs:
            folders_to_clean.append(runs)
        if uploads:
            folders_to_clean.append(uploads)
        if folders_to_clean:
            base_path = join(_d4p["mountpath"], username)
            for folder in folders_to_clean:
                path = check_for_sub_dir(base_path, folder, subfolder_uploads, subfolder_runs)
                cleanup_folder(path)
        return "Success", 200

    except (OSError, BaseException, Exception) as e:
        msg = "Something went wrong while cleaning up user's workspace: {}".format(e)
        return msg, 500


@app.route("/stop-job", methods=["GET"])
def stop_job():
    job_name = request.args.get("job_name")
    run_dir = request.args.get("run_dir")
    logger.info("Stopping job with name: {}".format(job_name))
    response = {"result": ""}
    job_name_parts = job_name.split("-")
    try:
        job_handler = JobPreparator(d4p_settings=_d4p, namespace=name_space, user_id=g.user_id, username=g.username,
                                    mountpath=_d4p["mountpath"], pod_prefix=job_name_parts[0],
                                    pod_suffix=job_name_parts[-1], token=request.args.get("access_token"),
                                    issuer=g.issuer, nodes=1, logger=logger, kind="kill", run_dir=run_dir)
        if job_handler.killed_job:
            response["result"] = "Job successfully deleted"
        else:
            response["result"] = "Something went wrong and job was not killed! Contact your admin!"
        return json.dumps(response), 200
    except (BaseException, Exception) as e:
        response["result"] = "{}".format(e)
        return json.dumps(response), 500


@app.before_request
def is_authenticated():
    """
    Function that is executed before any HTTP request to the Execution API. It requires a valid access token in order
    to allow the request execution. It retrieves the provided token from the request and performs an HTTP call to the
    dare-login component in order to check whether the token is valid. Once the dare-login returns a successful
    response, the username, user id and the issuer are stored in the g flask variable so as to be access by the
    relevant API function.

    The Execution API endpoints that do not require authentication should use the decorator exclude_from_auth.
    """
    if request.endpoint in app.view_functions:
        view_func = app.view_functions[request.endpoint]
        # if not hasattr(view_func, "exclude_from_auth"):
        if not hasattr(view_func, "exclude_from_auth"):
            if request.method == "GET" or "upload" in request.endpoint:
                request_data = request.args
            else:
                request_data = request.form
            token = request_data["access_token"]
            try:
                url = "{}/validate-token".format(LOGIN_URL)
                response = requests.post(url, data=json.dumps({"access_token": token}))
                if response.status_code == 200:
                    result = json.loads(response.text)
                    g.username = result["username"]
                    g.user_id = result["user_id"]
                    g.issuer = result["issuer"]
                else:
                    msg = "Token is expired or user has not signed in! {}".format(response.text)
                    return msg, 500
            except (ConnectionError, Exception) as e:
                print(e)
                msg = "Something went wrong! {}".format(e)
                return msg, 500
