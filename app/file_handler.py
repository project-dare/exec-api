import shutil
import zipfile
from os import remove
from os import system, mkdir, listdir, unlink
from os.path import join, exists, isfile, isdir, islink
from pathlib import Path

from webdav.client import Client as WebDavClient
from werkzeug.utils import secure_filename

RUNS = "runs"
UPLOADS = "uploads"
DEBUG = "debug"


def create_missing_folders_for_user(mount_path, token):
    """
    Creates all main user directories, i.e. uploads, runs and debug
    Inside the mount path, e.g. /home/mpiuser/sfs/ for each user a directory
    is creates e.g. /home/mpiuser/sfs/username/ and inside the user's directory
    the three main directories are created (uploads, runs and debug)

    Args
        | mount_path (str): the default base path in the Shared File System
        | token (str): authentication token of the user
    """
    if not exists(join(mount_path, token)):
        mkdir(join(mount_path, token))
    if not exists(join(mount_path, token, UPLOADS)):
        mkdir(join(mount_path, token, UPLOADS))
    if not exists(join(mount_path, token, RUNS)):
        mkdir(join(mount_path, token, RUNS))
    if not exists(join(mount_path, token, DEBUG)):
        mkdir(join(mount_path, token, DEBUG))


def b2drop_upload(data, logger):
    """
    Upload a single file in B2DROP

    Args
        | oc (owncloud.Client): owncloud to access user's B2DROP
        | token (str): the token to authenticate the user
        | remote_dir (str): the base remote dir of the user
        | local_file (str): the name / path to the local file to be uploaded

    Returns
        str: the link to the uploaded file
    """
    kind = data["upload_kind"]
    path = data["path"]
    if kind == "directory":
        dir_name = path.split('/')[-1]
        parent_path = Path(path)
        parent_dir = parent_path.parent
        zip_file = join(str(parent_dir), dir_name + ".zip")
        system("zip {} {}".format(zip_file, path))
        path = zip_file
    remote_dir = data.get("remote_dir", "/remote.php/webdav/DARE")
    remote_dir = "/remote.php/webdav/" + remote_dir
    logger.info("B2DROP directory: {}".format(remote_dir))
    logger.info("DARE platform path: {}".format(path))
    username = data.get("b2drop_username", None)
    password = data.get("b2drop_password", None)
    options = {
        'webdav_hostname': "https://b2drop.eudat.eu",
        'webdav_login': username,
        'webdav_password': password,
        'verbose': True
    }
    web_dav_client = WebDavClient(options)
    web_dav_client.verify = False
    web_dav_client.upload_async(remote_path=remote_dir + "/" + path.split('/')[-1], local_path=path)
    link = web_dav_client.publish(remote_dir + "/" + path.split('/')[-1])
    print("B2DROP link acquired: {}".format(link))
    return link


def list_user_folders(data, mountpath):
    ret = {}
    # Load request data
    username = data["username"]
    user_uploads_folder = join(mountpath, username, UPLOADS)
    user_runs_folder = join(mountpath, username, RUNS)
    user_debug_folder = join(mountpath, username, DEBUG)
    # Uploaded files
    upl_dir = listdir(user_uploads_folder)
    upl = []
    for directory in upl_dir:
        obj = {'path': join(user_uploads_folder, directory)}
        upl.append(obj)

    ret['uploads'] = upl
    # Run generated files
    run_dir = listdir(user_runs_folder)
    run = []
    for directory in run_dir:
        obj = {'path': join(user_runs_folder, directory)}
        if obj:
            run.append(obj)
    ret['run'] = run
    debug_dir = listdir(user_debug_folder)
    debug = []
    for directory in debug_dir:
        obj = {}
        if username in directory:
            obj['path'] = join(user_debug_folder, directory)
            debug.append(obj)
    ret['debug'] = debug
    return ret


def list_user_files(path):
    ret = {}
    # Uploaded files
    upl_dir = listdir(path)
    upl = []
    for _dir in upl_dir:
        obj = {'path': path + '/' + _dir}
        upl.append(obj)
    ret['files'] = upl
    return ret


def upload_file(data, file, mountpath):
    user_uploads_folder = join(mountpath, data["username"], UPLOADS)
    dataset_name = data["dataset_name"]
    path = data["path"]
    # Check for missing arguments
    if not (None in [dataset_name]):
        # File binary
        # Check file extension
        if file and _allowed_file(file.filename):
            # Upload
            filename = secure_filename(file.filename)
            # Check if out-dir exists
            try:
                file_folder = join(user_uploads_folder, path) if path else user_uploads_folder
                file_path = join(file_folder, filename)
                file.save(file_path)
                if filename.endswith(".zip"):
                    with zipfile.ZipFile(file_path, 'r') as zip_ref:
                        zip_ref.extractall(file_folder)
                    remove(file_path)
                elif filename.endswith(".rar"):
                    system('bash unrar.sh ' + file_path + ' ' + file_folder)
            # Create path and upload
            except (OSError, FileNotFoundError, Exception):
                if path:
                    if not exists(join(user_uploads_folder, path)):
                        mkdir(join(user_uploads_folder, path))
                    file.save(join(user_uploads_folder, path, filename))
                    if file.filename.endswith(".zip"):
                        with zipfile.ZipFile(join(user_uploads_folder, path, filename), 'r') as zip_ref:
                            zip_ref.extractall(join(user_uploads_folder, path))
                        remove(join(user_uploads_folder, path, filename))
                    elif file.filename.endswith(".rar"):
                        system('bash unrar.sh ' + join(user_uploads_folder, path, filename) + ' ' +
                               join(user_uploads_folder, path))
            return 'OK!', 200
        else:
            return "File extension: {} is not allowed".format(file.filename.split(".")[1]), 500
    else:
        return "Missing dataset_name argument", 500


def check_for_sub_dir(base_path, folder, subfolder_uploads, subfolder_runs):
    path = join(base_path, folder)
    if folder == "uploads" and subfolder_uploads:
        path = join(path, folder)
    elif folder == "runs" and subfolder_runs:
        path = join(path, folder)
    return path


def cleanup_folder(path):
    folders_to_be_cleaned = listdir(path)
    for fold in folders_to_be_cleaned:
        fold_path = join(path, fold)
        if isfile(fold_path) or islink(fold_path):
            unlink(fold_path)
        elif isdir(fold_path):
            shutil.rmtree(fold_path)
    for fold in folders_to_be_cleaned:
        fold_path = join(path, fold)
        if isfile(fold_path):
            remove(fold_path)
        elif isdir(fold_path):
            shutil.rmtree(fold_path)


# Check file extension before uploading
def _allowed_file(filename, allowed_extensions=('zip', 'rar')):
    """
    Checks which file formats are allowed to be uploaded in the platform
    Currently only the zip format is allowed for files to be uploded.

    Args
        | filename (str): the name of the file
        | allowed_extensions (tuple): all the allowed extensions

    Returns
        bool: True/False if the file extension is allowed
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in allowed_extensions
