from os import getcwd, mkdir
from os.path import join, exists

import requests


def download_docker_files():
    docker_folder = join(getcwd(), "docker_files")
    if not exists(docker_folder):
        mkdir(docker_folder)

    # get the latest docker files
    dockerfile = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-f4f/Dockerfile")
    with open(join(docker_folder, "Dockerfile"), "w") as f:
        f.write(dockerfile.text)

    entrypoint = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-f4f/entrypoint.sh")
    with open(join(docker_folder, "entrypoint.sh"), "w") as f:
        f.write(entrypoint.text)


def download_cwl_files():
    cwl_folder = join(getcwd(), "cwl_files")
    if not exists(cwl_folder):
        mkdir(cwl_folder)

    # get the workflow
    workflow = requests.get("https://gitlab.com/project-dare/dare-examples/-/raw/master/f4f/cwl/analytics.cwl")
    with open(join(cwl_folder, "analytics.cwl"), "w") as f:
        f.write(workflow.text)

    # get the spec file
    spec = requests.get("https://gitlab.com/project-dare/dare-examples/-/raw/master/f4f/cwl/spec.yaml")
    with open(join(cwl_folder, "spec.yaml"), "w") as f:
        f.write(spec.text)

    # get the python script for the execution
    python_script = requests.get("https://gitlab.com/project-dare/dare-examples/-/raw/master/f4f/cwl/analytics.py")
    with open(join(cwl_folder, "analytics.py"), "w") as f:
        f.write(python_script.text)
