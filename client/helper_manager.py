import json
import time
from os import getcwd, environ, walk
from os.path import join, exists, abspath
from pathlib import Path
from zipfile import ZipFile, ZIP_DEFLATED

import requests
import wget
import yaml
from IPython.display import clear_output


class DareManager:
    """
    This class integrates all the DARE services and exposes the necessary functionality to the users. DareManager allows
    the users to login to the platform, register dispel4py or CWL workflows, execute their applications, retrieve their
    files and logs, share them in B2DROP etc
    """
    # base urls
    DARE_PLATFORM_URL = ""
    LOGIN_URL_HOSTNAME = ""
    D4P_REGISTRY_HOSTNAME = ""
    WORKFLOW_REGISTRY_HOSTNAME = ""
    EXEC_API_HOSTNAME = ""

    # configuration & credentials
    config_file = None
    properties = {}
    username = ""
    password = ""
    email = ""
    user_id = ""
    full_name = ""
    b2drop_username = ""
    b2drop_password = ""
    token_issuer = ""
    token = ""
    refresh_token = ""
    expires_in = -1

    # d4p params
    impl_id = -1
    workspace_id = -1
    workspace_url = None
    pe_name = None

    # cwl params
    cwl_name = None
    cwl_version = None
    docker_name = None
    docker_tag = None

    # run params
    run_dir = None
    run_id = None
    job_name = None

    # api managers
    d4p_manager = None
    cwl_manager = None
    exec_manager = None

    def __init__(self, dare_platform_url, config_file="credentials.yaml"):
        """
        DareManager constructor. Uses the platform's domain to initialize the URLs to the DARE services. It also uses
        the configuration file to authenticate the user to the platform. If the file is not provided, DareManager will
        try to find user's session token and details from the environment, in case the execution is performed in the
        JupyterLab in the DARE platform.

        Args
            dare_platform_url: the domain / base URL to the platform e.g. https://platform.dare.scai.fraunhofer.de
        """
        self.DARE_PLATFORM_URL = dare_platform_url
        self.LOGIN_URL_HOSTNAME = dare_platform_url + "/dare-login"
        self.D4P_REGISTRY_HOSTNAME = dare_platform_url + "/d4p-registry"
        self.WORKFLOW_REGISTRY_HOSTNAME = dare_platform_url + "/workflow-registry"
        self.EXEC_API_HOSTNAME = dare_platform_url + "/exec-api"
        self.config_file = config_file
        self._init_credentials_from_file()
        if "DARE_ACCESS_TOKEN" in environ.keys():
            self.token = environ["DARE_ACCESS_TOKEN"]
            self.refresh_token = environ["DARE_REFRESH_TOKEN"]
            self.username = environ["DARE_USER_NAME"]
            self.email = environ["DARE_USER_EMAIL"]
            self.user_id = environ["DARE_USER_SUB"]
            self.full_name = environ["DARE_USER_FULLNAME"]
            self.refresh_access_token()
        self.login(self.token)
        self.d4p_manager = D4pManager(dare_platform_url=self.DARE_PLATFORM_URL, token=self.token,
                                      username=self.username)
        self.cwl_manager = CwlManager(dare_platform_url=self.DARE_PLATFORM_URL, token=self.token,
                                      username=self.username)
        self.exec_manager = ExecManager(dare_platform_url=self.DARE_PLATFORM_URL, token=self.token,
                                        username=self.username, b2drop_username=self.b2drop_username,
                                        b2drop_password=self.b2drop_password)

    def login(self, token=None):
        """
        Login function which calls the dare-login service in order to authenticate the user and acquire a session token.

        Returns
            dict: a dictionary containing the access and refresh tokens
        """
        url = self.LOGIN_URL_HOSTNAME + '/auth'
        headers = {"Content-Type": "application/json"}
        if not token:
            print("Trying to login using configuration file")
            data = self._login_params_with_credentials()
            r = requests.post(url=url, data=json.dumps(data), headers=headers)
            if r.status_code == 200:
                self._init_session_token(r)
            else:
                print("Could not authenticate user!")
        else:
            print("Trying to login using JupyterHub environmental variables")
            data = {"access_token": self.token}
            r = requests.post(url=url, data=json.dumps(data), headers=headers)
            if r.status_code != 200:
                print("Failed to validate the user from JupyterHub session. Trying with the configuration file...")
                data = self._login_params_with_credentials()
                r = requests.post(url=url, data=json.dumps(data), headers=headers)
                if r.status_code == 200:
                    self._init_session_token(r)
                else:
                    print("Could not authenticate user!")
            else:
                print("User was successfully validated using JupyterHub env variables!")

    def _login_params_with_credentials(self):
        return {
            "username": self.username,
            "password": self.password,
            "requested_issuer": self.token_issuer
        }

    def _init_session_token(self, response):
        response = json.loads(response.text)
        self.token = response["access_token"]
        self.refresh_token = response["refresh_token"]
        self.expires_in = response["expires_in"]
        environ["DARE_ACCESS_TOKEN"] = self.token
        environ["DARE_REFRESH_TOKEN"] = self.refresh_token

    def _init_credentials_from_file(self):
        if self.config_file and exists(self.config_file):
            with open(self.config_file, "r") as f:
                self.properties = yaml.safe_load(f)
                self.username = self.properties.get("username", None)
                self.password = self.properties.get("password", None)
                self.token_issuer = self.properties.get("issuer", None)
                self.b2drop_username = self.properties.get("b2drop_username", None)
                self.b2drop_password = self.properties.get("b2drop_password", None)

    def refresh_access_token(self):
        """
        Function to refresh a token. Uses the dare-login service to get a new access token
        """
        try:
            url = "{}/refresh-token".format(self.LOGIN_URL_HOSTNAME)
            response = requests.post(url, data=json.dumps({"refresh_token": self.refresh_token}))
            if response.status_code != 200:
                print(response.status_code, response.text)
                return response.status_code, response.text
            refresh_resp = json.loads(response.text)
            self.token = refresh_resp["access_token"]
            self.refresh_token = refresh_resp["refresh_token"]
            self.expires_in = refresh_resp["expires_in"]
        except (ConnectionError, Exception) as e:
            print(e)
            return 500, "Could not refresh token! {}".format(e)

    def register_d4p_workflow(self, name, code, pe_descr="", workspace_descr="", workspace_url=None, workspace_id=None,
                              delete_workspace=True):
        """
        Function to register a dispel4py workflow in the platform. It uses the D4pManager for the registration.

        Args
            | name (string): the PE name
            | code (string): the PE / workflow code
            | pe_descr (string): a description for the PE (optional)
            | workspace_descr (string): a description for your workspace (if you wish to delete the old and re-register
            a workspace)
            | workspace_id (int): optional, needs to be provided if there is nothing in the session and you do not
            want to delete your existing workspace
            | workspace_url (string): optional, needs to be provided if there is nothing in the session and you do not
            want to delete your existing workspace
            | delete_workspace (bool): True / False based on whether you want to delete your existing workspace or not

        Returns
            | tuple: the workspace ID and URL
        """
        self.pe_name = name
        if delete_workspace:
            try:
                self.d4p_manager.delete_workspace(self.username)
            # Progress check
            # if response.status_code == 204:
            #     print('Deleted workspace ' + name)
            # else:
            #     print('Delete workspace returned status code: ' + str(response.status_code))
            #     print(response.text)
            except (BaseException, Exception):
                pass
            workspace_url, workspace_id = self.d4p_manager.create_workspace(clone="", name=self.username,
                                                                            desc=workspace_descr)
            self.workspace_id = int(workspace_id)
            self.workspace_url = workspace_url
        else:
            if not self.workspace_url and self.workspace_id:
                if not workspace_id and not workspace_url:
                    print("You need to provide the workspace ID and URL! Session is empty")
                else:
                    self.workspace_id = workspace_id
                    self.workspace_url = workspace_url
        pe_url = self.d4p_manager.create_pe(desc=workspace_descr, name=name, workspace=self.workspace_url)
        self.impl_id = self.d4p_manager.create_peimpl(desc=pe_descr, code=code, parent_sig=pe_url, name=name,
                                                      workspace=self.workspace_url)
        return self.workspace_id, self.impl_id

    def register_docker(self, docker_params):
        """
        Function to register a docker in the CWL Workflow Registry. It uses the CwlManager for the registration.

        Args
            | docker_params (dict): the docker parameters should contain the following keys: docker_name, docker_tag,
            script_names (if you want to register scripts along with the Dockerfile), path - which is your local path
            to the files that you want to register

        Returns
            | dict: the json response containing the registered docker
        """
        self.docker_name = docker_params["docker_name"]
        self.docker_tag = docker_params["docker_tag"]
        try:
            self.cwl_manager.delete_docker(docker_name=self.docker_name, docker_tag=self.docker_tag)
        except (BaseException, Exception):
            pass
        script_names = docker_params["script_names"]
        path_to_files = docker_params["path"]
        if script_names:
            docker = self.cwl_manager.create_docker_env_with_scripts(docker_name=self.docker_name,
                                                                     docker_tag=self.docker_tag,
                                                                     script_names=script_names,
                                                                     path_to_files=path_to_files)
        else:
            docker = self.cwl_manager.create_docker_env(docker_name=self.docker_name, docker_tag=self.docker_tag,
                                                        docker_path=path_to_files)
        return docker

    def provide_docker_image_url(self, docker_params):
        """
        Function to update a registered docker once built. It should be used in order to provide a public URL of the
        image

        Args
            | docker_params (dict): the dictionary should contain the following keys: docker_name, docker_tag and url

        Returns
            | json response with the updated docker
        """
        return self.cwl_manager.provide_url(docker_name=docker_params["docker_name"],
                                            docker_tag=docker_params["docker_tag"],
                                            docker_url=docker_params["url"])

    def register_cwl(self, cwl_params, docker_params, register_docker=True):
        """
        Function to register a CWL workflow. It allows you to register a docker with its URL immediately without
        using the above functions. It uses the CwlManager for the registration.

        Args
            | cwl_params (dict): the dictionary should contain the keys: workflow_name, workflow_version, spec_name,
            path_to_cwls - which is your local path to the files that you want to register, workflow_part_data -
            optional key if you want to immediately register the CommandLineTools workflows along with your Workflow
            CWL
            | docker_params (dict): the dictionary should contain the keys: docker_name, docker_tag, script_names -
            optional key if you want to register bash or python scripts along with the Dockerfile, path - which is
            your local path to the files that you want to register. If you do not want to register a new docker, the
            dictionary should contain only the docker name and tag.
            | register_docker (bool): optional parameter to define if you want to use the docker_params in order to
            register a new docker. By default is set to True and registers the docker.

        Returns
            | json response with the created workflow (it also contains the docker and workflow parts)
        """
        self.docker_name = docker_params["docker_name"]
        self.docker_tag = docker_params["docker_tag"]
        if register_docker:
            try:
                self.cwl_manager.delete_docker(docker_name=self.docker_name, docker_tag=self.docker_tag)
            except (BaseException, Exception):
                pass
            script_names = docker_params["script_names"]
            path_to_files = docker_params["path"]
            docker = self.cwl_manager.create_docker_env_with_scripts(docker_name=self.docker_name,
                                                                     docker_tag=self.docker_tag,
                                                                     script_names=script_names,
                                                                     path_to_files=path_to_files)
            if docker[0] != 200:
                return docker
            else:
                url = docker_params["url"]
                resp = self.cwl_manager.provide_url(docker_name=self.docker_name, docker_tag=self.docker_tag,
                                                    docker_url=url)
                if resp[0] != 200:
                    return resp

        self.cwl_name = cwl_params["workflow_name"]
        self.cwl_version = cwl_params["workflow_version"]

        try:
            self.cwl_manager.delete_workflow(workflow_name=self.cwl_name, workflow_version=self.cwl_version)
        except (BaseException, Exception):
            pass

        spec_name = cwl_params["spec_name"]
        path_to_cwls = cwl_params["path_to_cwls"]
        workflow_part_data = cwl_params.get("workflow_part_data", {})
        workflow_resp = self.cwl_manager.create_workflow(workflow_name=self.cwl_name, workflow_version=self.cwl_version,
                                                         spec_name=spec_name, path_to_cwls=path_to_cwls,
                                                         docker_name=self.docker_name, docker_tag=self.docker_tag,
                                                         workflow_part_data=workflow_part_data)
        return workflow_resp

    def exec_d4p(self, nodes, impl_id=None, workspace_id=None, pe_name=None, image=None, reqs=None, pckg="main", **kw):
        """
        Function to execute a dispel4py workflow in the platform. It uses the ExecManager for the execution. The
        response of the execution is printed and saved in the session. It contains the run ID and directory and the
        job name.

        Args
            | nodes (int): the number of containers to be created for the execution
            | impl_id (int): optional if you run this function in a different session than the registration, it's the
            ID of your PEImpl
            | workspace_id (int): optional if you run this function in a different session than the registration, it's
            the ID of your workspace
            | pe_name (string): optional if you run this function in a different session than the registration, it's
            the name of your PEImpl
            | image (string): optional if you want to use a different exec-context-d4p image than the "latest"
            | reqs (string): optional - URL to a requirements.txt file with the necessary libs for the execution. If not
            provided we assume that you do not need any special libraries than those installed in the image
            | pckg (string): optional - it's the package name, by default set to main
            | kw (dict): optional - additional d4p arguments e.g. inputdata etc

        """
        if self.workspace_id > -1 and not workspace_id:
            workspace_id = self.workspace_id
        if self.impl_id > -1 and not impl_id:
            impl_id = self.impl_id
        if self.pe_name and not pe_name:
            pe_name = self.pe_name
        if self.workspace_id <= -1 and not workspace_id:
            print("You did not provide the workspace ID and DareManager has nothing stored in the session")
            return
        if self.impl_id <= -1 and not impl_id:
            print("You did not provide the PE ID and DareManager has nothing stored in the session")
            return
        if not self.pe_name and not pe_name:
            print("You did not provide the PE name and DareManager has nothing stored in the session")
            return
        resp = self.exec_manager.submit_d4p(n_nodes=nodes, impl_id=impl_id, workspace_id=workspace_id, pe_name=pe_name,
                                            pckg=pckg, image=image, reqs=reqs, **kw)
        if resp:
            print(resp)
            resp = json.loads(resp[1])
            self.run_dir = resp["run_dir"]
            self.run_id = resp["run_id"]
            self.job_name = resp["job_name"]

    def exec_cwl(self, nodes=1, workflow_name=None, workflow_version=None, input_data=None, save_workflow=None):
        """
        Function to execute a CWL workflow in the DARE platform. It uses the ExecManager for the execution. The
        response of the execution contains the run ID and directory and the job name.

        Args
            | nodes (int): optional, the number of containers to be created for the execution. By default is set to 1.
            | workflow_name (string): optional, should be provided only when this function is executed in a different
            session than the registration of the CWL workflow.
            | workflow_version (string): optional, should be provided only when this function is executed in a different
            session than the registration of the CWL workflow.
            | input_data (dict): optional, a dictionary containing input parameters for your workflow. In the image,
            they are set as environmental variable with the name INPUT_DATA
        """
        if self.cwl_name and not workflow_name:
            workflow_name = self.cwl_name
        if self.cwl_version and not workflow_version:
            workflow_version = self.cwl_version

        if not self.cwl_name and not workflow_name:
            print("You have not provided a workflow name and there is nothing stored in the session!")
            return
        if not self.cwl_version and not workflow_version:
            print("You have not provided a workflow version and there is nothing stored in the session!")
            return
        resp = self.exec_manager.submit_cwl(workflow_name=workflow_name, workflow_version=workflow_version,
                                            input_data=input_data, nodes=nodes, save_workflow=save_workflow)
        print(resp)
        if resp[0] == 200:
            resp = json.loads(resp[1])
            self.run_id = resp["run_id"]
            self.run_dir = resp["run_dir"]
            self.job_name = resp["job_name"]

    def monitor_job(self):
        """
        Function that prints the running containers for a specific user
        """
        self.exec_manager.monitor()

    def stop_job(self, job_name=None, run_dir=None):
        """
        Function to stop a running job in the platform. The run directory and job name returned by the execution
        function are stored in the session. If you are running this function in the same session as the execution,
        you do not need to pass these parameters.

        Args
            | job_name (string): the name of the running job returned by the execution function
            | run_dir (string): the name of the execution directory returned by the execution function
        """
        if self.job_name:
            job_name = self.job_name
        if self.run_dir:
            run_dir = self.run_dir
        if not self.run_dir and not run_dir:
            print("Provide the run directory associated with the run")
            return
        if not self.job_name and not job_name:
            print("Provide the job name! There is nothing in the cache!")
            return
        response = self.exec_manager.stop_job(job_name=job_name, run_dir=run_dir)
        print("Status: {}".format(response[0]))
        print("Response: {}".format(response[1]))

    def list_workspace(self, num_dirs=None, result="print"):
        """
        Function to list a user's uploaded files and files produced by the executions.

        Args
            | num_dirs (int): optional parameter to limit the number of the directories that are printed
            | result (string): select if you want the response to be returned or printed
        """
        resp = self.exec_manager.list_folders(num_run_dirs=num_dirs)
        if result == "print":
            self.exec_manager.folders_pretty_print(json.loads(resp), num_run_dirs=num_dirs)
        else:
            return resp

    def list_exec_folder(self, run_dir=None, kind="resp"):
        """
        Function to list the content of the output directory of a specific execution directory. If you run this
        function in the same session as the execution, the run_dir parameter is stored in the session and you do not
        need to provide it.

        Args
            | run_dir (string): optional, the name of the execution directory
            | kind (string): optional, select whether the response should be returned or printed
        """
        if self.run_dir and not run_dir:
            run_dir = self.run_dir
        if not self.run_dir and not run_dir:
            print("You did not specify a run directory and DareManager does not have a run dir in the session")
            return
        remote_path = "/home/mpiuser/sfs/{}/runs/{}/output".format(self.username, run_dir)
        resp = self.exec_manager.list_folder_files(remote_path=remote_path)
        if kind == "resp":
            return resp
        else:
            self.exec_manager.files_pretty_print(json.loads(resp))

    def list_upload_folder(self, sub_folder=None):
        """
        Function to list the content of a specific sub directory of the "uploads" folder.

        Args
            | sub_folder (string): the name of the sub folder that you want to list
        """
        remote_path = "/home/mpiuser/sfs/{}/uploads/{}".format(self.username, sub_folder) if sub_folder else \
            "/home/mpiuser/sfs/{}/uploads".format(self.username)
        resp = self.exec_manager.list_folder_files(remote_path=remote_path)
        self.exec_manager.files_pretty_print(json.loads(resp))

    def download_file(self, filename, kind="run", directory=None, local_path=getcwd()):
        """
        Function to download a file from the platform. If you set kind to run, which means that you want a file
        produced by an execution, and you are in the same session as the execution, you do not need to pass the
        directory parameter.

        Args
            | filename (string): the name of the file to be downloaded
            | kind (string): run or upload, defines the location of the file (uplaods or runs directories)
            | directory (string): optional, the name of the directory (run dir or sub folder of the uploads folder)
            | local_path (string): the location in your PC where the file will be stored
        """
        if kind == "run":
            if self.run_dir and not directory:
                directory = self.run_dir
            if not self.run_dir and not directory:
                print("You have not specified a run directory and DareManager has nothing in the session")
                return
            remote_path = "/home/mpiuser/sfs/{}/runs/{}/output/{}".format(self.username, directory, filename)
        else:
            remote_path = "/home/mpiuser/sfs/{}/uploads/".format(self.username)
            if directory:
                remote_path += directory + "/"
            remote_path += filename
        local_path = local_path + "/" + filename
        resp = self.exec_manager.download(remote_path=remote_path, local_path=local_path)
        print(resp)

    def upload_file(self, filename, remote_path, local_path=getcwd()):
        """
        Function to upload a single file to the platform.

        Args
            | filename (string): the name of the file to be uploaded
            | local_path (string): a directory inside your current working directory (optional) where the file is stored
            | remote_path (string): a subfolder of the uploads folder in the platform

        Returns
            | json response of the upload endpoint
        """
        # upload the json file with the parameters

        # zip_fd = ZipFile('input.zip', "w", ZIP_DEFLATED)
        src = join(getcwd(), local_path, filename) if local_path != getcwd() else \
            abspath(join(local_path, filename))
        zip_fd = ZipFile('input.zip', "w", ZIP_DEFLATED)
        zip_fd.write(src, filename)
        zip_fd.close()
        return self.exec_manager.upload(remote_path=remote_path, local_path='input.zip')

    def upload_dir(self, remote_path, local_path=getcwd()):
        """
        Function to upload a directory with its content in the platform.

        Args
            | remote_path (string): a subfolder of the uploads folder in the platform
            | local_path (string): absolute path to the directory that you want to upload

        Returns
            | json reponse from the upload endpoint
        """
        path = Path(local_path)
        path = str(path)
        rel_path = path.split("/")[-1]
        zip_name = rel_path + ".zip"
        zip_fd = ZipFile(zip_name, "w", ZIP_DEFLATED)
        self._zipdir(path, zip_fd, rel_path)
        zip_fd.close()
        return self.exec_manager.upload(remote_path=remote_path, local_path=zip_name)

    def _zipdir(self, path, ziph, parent):
        """
        Protected function of the DareManager, that creates a zip file containing the files or folders to be
        uploaded to the platform.

        Args
            | path (string): the path to the file or folder
            | ziph (ZipFile): a ZipFile object
            | parent (string): the name of the parent directory
        """
        # ziph is zipfile handle
        for root, dirs, files in walk(path):
            for directory in dirs:
                self._zipdir(join(root, directory), ziph, join(parent, directory))
            for file in files:
                ziph.write(join(root, file), join(parent, file))

    def b2drop_share(self, kind, dare_path_kind="run", dare_directory=None, filename=None, remote_dir=None):
        """
        Function to upload files in B2DROP

        Args
            | kind (string): file or directory
            | dare_path_kind (string): run or upload
            | dare_directory (string): the run directory of the uplaod's subfolder
            | filename (string): the name of the file to be uploaded (if the kind is set to file)
            | remote_dir (string): the name of the folder in B2DROP, you do not need to include "/remote.php/webdav/"
        """
        if not self.b2drop_username or not self.b2drop_password:
            self._init_b2drop_credentials()
        dare_path = "/home/mpiuser/sfs/{}/".format(self.username)
        dare_path += "runs/" if dare_path_kind == "run" else "uploads/"
        if dare_path_kind == "run":
            if self.run_dir and not dare_directory:
                dare_directory = self.run_dir
            if not self.run_dir and not dare_directory:
                print("There is no run directory in the cache! Provide the dare_directory parameter!")
                return
            dare_path += dare_directory
            if filename:
                dare_path += "/output/" + filename
        else:
            if dare_directory:
                dare_path += dare_directory
            if filename:
                dare_path += "/" + filename
        return self.exec_manager.send2drop(path=dare_path, kind=kind, username=self.b2drop_username,
                                           password=self.b2drop_password, remote_dir=remote_dir)

    def _init_b2drop_credentials(self):
        if self.config_file and exists(self.config_file):
            with open(self.config_file, "r") as f:
                properties = yaml.safe_load(f)
                self.b2drop_username = properties.get("b2drop_username", None)
                self.b2drop_password = properties.get("b2drop_password", None)
                self.exec_manager.b2drop_username = self.b2drop_username
                self.exec_manager.b2drop_password = self.b2drop_password
            if not self.b2drop_username or not self.b2drop_password:
                print("Configuration file did not contain any username/password for B2DROP!")
                exit(-1)
        else:
            print("No configuration file is provided! Can't find B2DROP credentials")
            exit(-1)

    def cleanup_workspace(self, uploads=False, runs=False, subfolder_runs=None, subfolder_uploads=None):
        """
        Function to clear the content of the user's uploads and runs folders

        Args
            | uploads (bool): True / False based on whether you want to delete the content of the uploads folder
            | runs (bool): True / False based on whether you want to delete the content of the uploads folder
            | subfolder_runs (string): the name of a specific run folder to delete, should be combined with runs=True
            | subfolder_uploads (string): the name of a specific upload folder to delete, should be combined with
            uploads=True
        """
        return self.exec_manager.cleanup_dirs(uploads=uploads, runs=runs, subfolder_runs=subfolder_runs,
                                              subfolder_uploads=subfolder_uploads)

    def get_file_content(self, filename, run_dir=None, out_dir=None, path=None):
        """
        Function to get the content of a json file (used by f4f use case)

        Args
            | filename (string): the name of the file which should be loaded
            | run_dir (string): optional, based on whether the name of the dir is in the session
            | out_dir (string): optional, if the directory is different than the "output" directory inside the
            run_dir
            | path (string): do not provide run and output dirs if you wish to provide full path to the directory
            that you want

        Returns
            | json response with the content of the file
        """
        if not path:
            if self.run_dir and not run_dir:
                run_dir = self.run_dir
            if not self.run_dir and not run_dir:
                print("Provide a run directory! There is no run directory in the session")
                return
            if not out_dir:
                out_dir = "output"
            return self.exec_manager.get_file_content(filename=filename, run_dir=run_dir, out_dir=out_dir)
        else:
            return self.exec_manager.get_file_content(filename=filename, path=path)


class D4pManager:
    """
    This class is used in order to interact with the d4p-registry service of the DARE platform.
    """

    D4P_REGISTRY_HOSTNAME = ""
    token = ""
    username = ""

    def __init__(self, dare_platform_url, token, username):
        """
        D4pManager constructor. Needs the domain / base URL to the platform, the session token and the username.
        """
        self.D4P_REGISTRY_HOSTNAME = dare_platform_url + "/d4p-registry"
        self.token = token
        self.username = username

    def get_workspace(self, name):
        """
        Uses the Dispel4py Information Registry to find a workspace by name

        Args
            | name (str): the name of the workspace
        """
        # Get json response
        req = requests.get(self.D4P_REGISTRY_HOSTNAME + '/workspaces/',
                           params={"access_token": self.token, "name": name})
        resp_json = json.loads(req.text)
        if type(resp_json) == dict:
            return resp_json["url"], resp_json["id"]
        elif type(resp_json) == list:
            # Iterate and retrieve
            return ([i['url'] for i in resp_json if i['name'] == name][0],
                    [i['id'] for i in resp_json if i['name'] == name][0])

    def create_workspace(self, clone, name, desc):
        """
        Function that uses the Dispel4py Information Registry to create a new workspace

        Args
            | clone (str): defines if the workspace will be a clone of some existing workspace
            | name (str): the name of the workspace
            | desc (str): a description for the workspace
        """
        # Prepare data for posting
        data = {
            "clone_of": clone,
            "name": name,
            "access_token": self.token,
            "description": desc
        }
        # Request for d4p registry api
        _r = requests.post(self.D4P_REGISTRY_HOSTNAME + '/workspaces/', data=data)

        # Progress check
        if _r.status_code == 201:
            print('Added workspace: ' + name)
            return self.get_workspace(name)
        else:
            print('Add workspace resource returns status_code: ' +
                  str(_r.status_code))
            return self.get_workspace(name)

    def delete_workspace(self, name):
        """
        Function that uses the Dispel4py Information Registry to delete an existing workspace

        Args
            | name (str): the name of the workspace
        """
        workspace_url, wid = self.get_workspace(name)
        _r = requests.delete(self.D4P_REGISTRY_HOSTNAME + '/workspaces/' + str(wid) + '/',
                             data={"access_token": self.token})
        return _r

    # Create ProcessingElement using d4p registry api
    def create_pe(self, name, desc, workspace, conn=None, pckg="main", clone="", peimpls=None):
        """
        Uses the Dispel4py Information Registry to create a new PE

        Args
            | desc (str): a description for the PE
            | name (str): the name of the PE
            | pckg (str): the package name
            | workspace (str): the name of the associated workspace
            | clone (str): defines if it's a clone of an existing PE
            | peimpls (list): provides the peimpls
        """

        # Prepare data for posting
        data = {
            "description": desc,
            "name": name,
            "connections": conn,
            "pckg": pckg,
            "workspace": workspace,
            "clone_of": clone,
            "access_token": self.token,
            "peimpls": peimpls
        }
        # Request for d4p registry api
        _r = requests.post(self.D4P_REGISTRY_HOSTNAME + '/pes/', data=data)
        # Progress check
        if _r.status_code == 201:
            print('Added Processing Element: ' + name)
            return json.loads(_r.text)['url']
        else:
            print('Add Processing Element resource returns status_code: ' +
                  str(_r.status_code))

    # Create ProcessingElement Implementation using d4p registry api
    def create_peimpl(self, name, desc, code, parent_sig, workspace, clone="", pckg="main"):
        """
        Creates a new PEImpl using the Dispel4py Information Registry

        Args
            | desc (str): the description of the PE
            | code (str): the code of the workflow
            | parent_sig (str): the PE signature
            | pckg (str): the package name
            | name (str): the PE name
            | workspace (str): the workspace name
            | clone (str): defines if it's a PEImpl clone
        """
        # Prepare data for posting
        data = {
            "description": desc,
            "access_token": self.token,
            "code": code,
            "parent_sig": parent_sig,
            "pckg": pckg,
            "name": name,
            "workspace": workspace,
            "clone_of": clone
        }
        # Request for d4p registry api, verify=False is only for demo purposes
        # nginx is too slow on response, open issue
        _r = requests.post(self.D4P_REGISTRY_HOSTNAME + '/peimpls/', data=data, verify=False)
        # Progress check
        if _r.status_code == 201:
            print('Added Processing Element Implementation: ' + name)
            return json.loads(_r.text)['id']
        else:
            print('Add Processing Element Implementation resource returns \
                    status_code: ' + str(_r.status_code))


class CwlManager:
    """
    Class to interact with the CWL Workflow Registry of the DARE platform
    """
    CWL_REGISTRY_URL = ""
    username = ""
    token = ""

    def __init__(self, dare_platform_url, username, token):
        """
        CwlManager constructor. Needs the domain / base URL, the username and the session token.
        """
        self.CWL_REGISTRY_URL = dare_platform_url + "/workflow-registry"
        self.username = username
        self.token = token

    def create_docker_env_with_scripts(self, docker_name, docker_tag, script_names, path_to_files):
        """
            Register an entire docker environment to the DARE platform. Under the path_to_files directory you should
            already have stored a Dockerfile and the relevant bash or python scripts

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment
                | script_names (list): a list of names for your docker scripts
                | path_to_files (str): the path to the Dockerfile and scripts

            Returns
                tuple: the response status code and the response content
        """
        if not exists(join(path_to_files, "Dockerfile")):
            return "Dockerfile does not exist in folder {}".format(path_to_files)
        data = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "script_names": script_names,
            "access_token": self.token
        }
        with open(join(path_to_files, "Dockerfile"), 'r') as d:
            dockerfile = d.read()
        files = {"dockerfile": dockerfile}
        if script_names:
            for script_name in script_names:
                with open(join(path_to_files, script_name), 'r') as s:
                    script = s.read()
                files[script_name] = script
        data["files"] = files
        response = requests.post(self.CWL_REGISTRY_URL + "/docker/", json=data)
        return response.status_code, response.text

    def create_docker_env(self, docker_name, docker_tag, docker_path):
        """
            Create a docker environment with a Dockerfile (without any other scripts)

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment
                | docker_path (str): the path to the Dockerfile (e.g. /home/user/docker/Dockerfile)

            Returns
                tuple: the response status code and the response content
        """
        if not exists(docker_path):
            return "File does not exist"
        data = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "access_token": self.token
        }
        with open(docker_path, 'r') as df:
            docker_content = df.read()
        files = {"dockerfile": docker_content}
        data["files"] = files
        response = requests.post(self.CWL_REGISTRY_URL + "/docker/", json=data)
        return response.status_code, response.text

    def update_docker(self, update, docker_name, docker_tag, path_to_docker=None):
        """
            Function to update an existing docker environment. You should provide a dict (parameter update) with the
            fields to be updated. Valid keys to the dict are: name, tag, url, dockerfile

            Args
                | update (dict): docker fileds to be updated in the registry
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment
                | path_to_docker (str): the path to the Dockerfile (e.g. /home/user/docker/Dockerfile) - optional,
                should be used only if you need to update the Dockerfile

            Returns
                tuple: the response status code and the response content
        """
        data = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "access_token": self.token,
            "update": update
        }
        if path_to_docker:
            with open(path_to_docker, "r") as df:
                docker_content = df.read()
            files = {"dockerfile": docker_content}
            data["files"] = files
        response = requests.post(self.CWL_REGISTRY_URL + "/docker/update_docker/", json=data)
        return response.status_code, response.text

    def provide_url(self, docker_name, docker_tag, docker_url):
        """
            Function to update the URL to a public docker image for a specific docker environment. The docker env should
            already be available in the registry.

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment
                | docker_url (str): the URL to the public docker image

            Returns
                tuple: the response status code and the response content
        """
        data = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "docker_url": docker_url,
            "access_token": self.token
        }
        response = requests.post(self.CWL_REGISTRY_URL + "/docker/provide_url/", data=data)
        return response.status_code, response.text

    def delete_docker(self, docker_name, docker_tag):
        """
            Function to delete a docker environment (Dockerfile and associated scripts)

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment

            Returns
                tuple: the response status code and the response content
        """
        data = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "access_token": self.token
        }
        response = requests.delete(self.CWL_REGISTRY_URL + "/docker/delete_docker/", data=data)
        return response.status_code, response.text

    def get_docker_by_name_and_tag(self, docker_name, docker_tag):
        """
            Retrieve a docker environment by name and tag

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment

            Returns
                tuple: the response status code and the response content
        """
        data = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "access_token": self.token
        }
        response = requests.get(self.CWL_REGISTRY_URL + "/docker/bynametag", params=data)
        return response.status_code, response.text

    def get_docker_by_user(self, username=None):
        """
            Retrieve a docker env by user. You can specify a user (if it's not you) otherwise the system will identify
            you from the provided token.

            Args
                | username (str): optional, the username associated with the docker

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/docker/byuser"
        data = {"access_token": self.token}
        if username:
            data["requested_user"] = username
        response = requests.get(url, params=data)
        return response.status_code, response.text

    def download_docker(self, docker_name, docker_tag, local_path):
        """
            Function to download a zip file containing the Dockerfile and the relevant scripts

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/docker/download"
        params = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "access_token": self.token
        }
        try:
            myfile = requests.get(url, params=params)
            with open(local_path, "wb") as f:
                f.write(myfile.content)
            return "File successfully downloaded in {}".format(local_path)
        except (FileNotFoundError, Exception) as e:
            return "An error occurred while downloading the file: {}".format(e)

    # **************************** Docker Scripts **********************************
    def add_script_to_existing_docker(self, docker_name, docker_tag, script_name, path_to_script):
        """
            Function to register a script (bash or python) to an existing registered docker environment

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment
                | script_name (str): the name of the script
                | path_to_script (str): path to the folder that the script is stored (e.g. /home/user/docker/)

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/scripts/add/"
        data = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "script_name": script_name,
            "access_token": self.token
        }
        with open(join(path_to_script, script_name), "r") as s:
            script_content = s.read()
        files = {script_name: script_content}
        data["files"] = files
        response = requests.post(url, json=data)
        return response.status_code, response.text

    def edit_script_in_existing_docker(self, docker_name, docker_tag, script_name, path_to_script):
        """
            Function to update a script (bash or python) to an existing registered docker environment

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment
                | script_name (str): the name of the script
                | path_to_script (str): path to the folder that the script is stored (e.g. /home/user/docker/)

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/scripts/edit/"
        data = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "script_name": script_name,
            "access_token": self.token
        }
        with open(join(path_to_script, script_name), "r") as s:
            script_content = s.read()
        files = {script_name: script_content}
        data["files"] = files
        response = requests.post(url, json=data)
        return response.status_code, response.text

    def delete_script_in_docker(self, docker_name, docker_tag, script_name):
        """
            Function to delete a script of a docker env

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment
                | script_name (str): the name of the script

            Returns
                    tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/scripts/delete/"
        data = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "script_name": script_name,
            "access_token": self.token
        }
        response = requests.delete(url, data=data)
        return response.status_code, response.text

    def get_script_by_name(self, docker_name, docker_tag, script_name):
        """
            Function to get a script of a docker env by name

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment
                | script_name (str): the name of the script

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/scripts/byname"
        data = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "script_name": script_name,
            "access_token": self.token
        }
        response = requests.get(url, params=data)
        return response.status_code, response.text

    def download_script(self, docker_name, docker_tag, script_name, local_path):
        """
            Function to download a script of a docker env by name

            Args
                | docker_name (str): the name of your docker environment
                | docker_tag (str): the tag of your docker environment
                | script_name (str): the name of the script

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/scripts/download"
        params = {
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "script_name": script_name,
            "access_token": self.token
        }
        try:
            myfile = requests.get(url, params=params)
            with open(local_path, "wb") as f:
                f.write(myfile.content)
            return "File successfully downloaded in {}".format(local_path)
        except (FileNotFoundError, Exception) as e:
            return "An error occurred while downloading the file: {}".format(e)

    # ********************************* Workflows *****************************************
    def create_workflow(self, workflow_name, workflow_version, spec_name, path_to_cwls,
                        docker_name, docker_tag, workflow_part_data=None):
        """
            Registers a CWL workflow and its parts (if provided) to the registry.

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow
                | spec_name (str): the name of the spec yaml file
                | path_to_cwls (str): path to the folder where the CWL files are stored (locally)
                | docker_name (str): the name of the associated docker env
                | docker_tag (str): the tag of the associated docker env
                | workflow_part_data (list): name and yaml file name of each of the CWL of class CommandLineTool

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflows/"
        data = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "docker_name": docker_name,
            "docker_tag": docker_tag,
            "spec_file_name": spec_name,
            "workflow_part_data": workflow_part_data,
            "access_token": self.token
        }
        with open(join(path_to_cwls, workflow_name), "r") as wp:
            workflow_content = wp.read()
        with open(join(path_to_cwls, spec_name), "r") as sp:
            spec_content = sp.read()
        files = {"workflow_file": workflow_content, "spec_file": spec_content}
        if workflow_part_data:
            for workflow_part in workflow_part_data:
                with open(join(path_to_cwls, workflow_part["name"]), "r") as sc:
                    script_content = sc.read()
                files[workflow_part["name"]] = script_content
                if "spec_name" in workflow_part.keys():
                    with open(join(path_to_cwls, workflow_part["spec_name"]), "r") as sp:
                        spec_content = sp.read()
                    files[workflow_part["spec_name"]] = spec_content
        data["files"] = files
        response = requests.post(url, json=data)
        return response.status_code, response.text

    def update_workflow(self, workflow_name, workflow_version, update, workflow_path=None, specpath=None):
        """
            Updates a CWL workflow to the registry.

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow
                | update (dict): the workflow's fields to be updated. Valid fields are: name, version, spec_name
                | workflow_path (str): optional, the path to the CWL of class Workflow (only for file update)
                | specpath (str): the path to the yaml file, optional (only if you need to update the file)

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflows/update_workflow/"
        data = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "access_token": self.token,
            "update": update
        }
        if workflow_path:
            with open(workflow_path, "r") as f:
                file_content = f.read()
        if specpath:
            with open(specpath, "r") as sp:
                spec_content = sp.read()
        if file_content or spec_content:
            files = {}
            if file_content:
                files["workflow_file"] = file_content
            if spec_content:
                files["spec_file"] = spec_content
            data["files"] = files
        response = requests.post(url, json=data)
        return response.status_code, response.text

    def update_associated_docker(self, workflow_name, workflow_version, docker_name, docker_tag):
        """
            Function to update the associated docker environment for a specific workflow

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow
                | docker_name (str): the name of the associated docker env
                | docker_tag (str): the tag of the associated docker env

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflows/update_docker/"
        data = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "access_token": self.token,
            "docker_name": docker_name,
            "docker_tag": docker_tag
        }
        response = requests.post(url, data=data)
        return response.status_code, response.text

    def delete_workflow(self, workflow_name, workflow_version):
        """
            Deletes a CWL workflow and its parts (if provided) from the registry.

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflows/delete_workflow/"
        data = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "access_token": self.token
        }
        response = requests.delete(url, data=data)
        return response.status_code, response.text

    def get_workflow_by_name_and_version(self, workflow_name, workflow_version):
        """
            Function to retrieve a CWL workflow by name and version

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflows/bynameversion"
        data = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "access_token": self.token
        }
        response = requests.get(url, params=data)
        return response.status_code, response.text

    def download_workflow(self, workflow_name, workflow_version, local_path, dockerized=None):
        """
            Function to download a CWL workflow (class Workflow) and its CWL parts (class CommandLineTool), the relevant
            yaml files (spec files) and, if dockerized is set to True, it also downloads the Dockerfile and scripts
            (bash or python) of the associated docker container

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow
                | local_path (str): the path where the zip file will be stored
                | dockerized (bool): true to download the Dockerfile and scripts along with the workflow files. Leave it
                empty if you want only the CWL and yaml files

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflows/download"
        params = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "access_token": self.token
        }
        if dockerized:
            params["dockerized"] = True
        myfile = requests.get(url, params=params)
        if myfile.status_code == 200:
            with open(local_path, "wb") as f:
                f.write(myfile.content)
            return "File successfully downloaded in {}".format(local_path)
        else:
            return myfile.status_code, myfile.text

    # ******************************* Workflow Parts ********************************
    def add_workflow_part(self, workflow_name, workflow_version, workflow_part_name, workflow_part_version,
                          path_to_scripts, spec_name=None):
        """
            Registers a CWL workflow part (class CommandLineTool) to the registry. Relevant yaml file can also be
            stored, if different yaml files are used.

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow
                | workflow_part_name (str): the name of the CWL of class CommandLineTool to be registered
                | workflow_part_version (str): the version of the CWL of class CommandLineTool to be registered
                | path_to_scripts (str): the path to the folder containing the CWL files
                | spec_name (str): optional, use it only if you have different yaml file for each CWL

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflow_parts/add/"
        data = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "workflow_part_name": workflow_part_name,
            "workflow_part_version": workflow_part_version,
            "access_token": self.token
        }
        with open(join(path_to_scripts, workflow_part_name), "r") as s:
            script_content = s.read()
        files = {workflow_part_name: script_content}
        if spec_name:
            data["spec_name"] = spec_name
            with open(join(path_to_scripts, spec_name), "r") as sp:
                spec = sp.read()
            files[spec_name] = spec
        data["files"] = files
        response = requests.post(url, json=data)
        return response.status_code, response.text

    def edit_workflow_part(self, workflow_name, workflow_version, workflow_part_name, workflow_part_version,
                           update, spec_name=None, path_to_files=None):
        """
            Updates a CWL workflow (class CommandLineTool) to the registry.

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow
                | workflow_part_name (str): the name of the CWL of class CommandLineTool to be updated
                | workflow_part_version (str): the version of the CWL of class CommandLineTool to be updated
                | update (dict): the workflow's fields to be updated. Valid fields are: name, version, spec_name
                | spec_name (str): optional, use it only if you have different yaml file for each CWL
                | path_to_files (str): optional, use it only when you want to update the actual files and not only the
                workflow object's fields

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflow_parts/edit/"
        data = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "workflow_part_name": workflow_part_name,
            "workflow_part_version": workflow_part_version,
            "access_token": self.token,
            "update": update
        }

        if path_to_files:
            with open(join(path_to_files, workflow_part_name), "r") as f:
                file_content = f.read()
            files = {workflow_part_name: file_content}
            if spec_name:
                data["spec_name"] = spec_name
                with open(join(path_to_files, spec_name), "r") as sp:
                    spec_content = sp.read()
                files[spec_name] = spec_content
            data["files"] = files
        response = requests.post(url, json=data)
        return response.status_code, response.text

    def delete_workflow_part(self, workflow_name, workflow_version, workflow_part_name,
                             workflow_part_version):
        """
            Deletes a CWL workflow (class CommandLineTool) from the registry.

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow
                | workflow_part_name (str): the name of the CWL of class CommandLineTool to be deleted
                | workflow_part_version (str): the version of the CWL of class CommandLineTool to be deleted

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflow_parts/delete/"
        data = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "workflow_part_name": workflow_part_name,
            "workflow_part_version": workflow_part_version,
            "access_token": self.token
        }
        response = requests.delete(url, data=data)
        return response.status_code, response.text

    def get_workflow_part_by_name_and_version(self, workflow_name, workflow_version, workflow_part_name,
                                              workflow_part_version):
        """
            Function to retrieve a CWL of class CommandLineTool by name, tag and CWL name & version of the CWL of class
            Workflow

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow
                | workflow_part_name (str): the name of the CWL of class CommandLineTool to be retrieved
                | workflow_part_version (str): the version of the CWL of class CommandLineTool to be retrieved

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflow_parts/bynameversion"
        data = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "workflow_part_name": workflow_part_name,
            "workflow_part_version": workflow_part_version,
            "access_token": self.token
        }
        response = requests.get(url, params=data)
        return response.status_code, response.text

    def download_workflow_part(self, workflow_name, workflow_version, workflow_part_name,
                               workflow_part_version, local_path):
        """
            Downloads a CWL of class CommandLineTool and its associated yaml file if exists.

            Args
                | workflow_name (str): the name of the CWL with class Workflow
                | workflow_version (str): the version of the CWL with class Workflow
                | workflow_part_name (str): the name of the CWL of class CommandLineTool to be retrieved
                | workflow_part_version (str): the version of the CWL of class CommandLineTool to be retrieved
                | local_path (str): the path to store the zip file / should contain the zip filename as well

            Returns
                tuple: the response status code and the response content
        """
        url = self.CWL_REGISTRY_URL + "/workflow_parts/download"
        params = {
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "workflow_part_name": workflow_part_name,
            "workflow_part_version": workflow_part_version,
            "access_token": self.token
        }
        try:
            myfile = requests.get(url, params=params)
            with open(local_path, "wb") as f:
                f.write(myfile.content)
            return "File successfully downloaded in {}".format(local_path)
        except (FileNotFoundError, Exception) as e:
            return "An error occurred while downloading the file: {}".format(e)


class ExecManager:
    """
    Class to interact with the DARE Execution API.
    """

    EXEC_API_URL = ""
    token = ""
    username = ""
    b2drop_username = ""
    b2drop_password = ""

    def __init__(self, dare_platform_url, token, username, b2drop_username, b2drop_password):
        """
        ExecManager constructor. Needs the domain / base URL, the session token, the username in the DARE platform and
        the B2DROP credentials
        """
        self.EXEC_API_URL = dare_platform_url + "/exec-api"
        self.token = token
        self.username = username
        self.b2drop_username = b2drop_username
        self.b2drop_password = b2drop_password

    # Spawn mpi cluster and run dispel4py workflow
    def submit_d4p(self, impl_id, workspace_id, pe_name, n_nodes, pckg="main", image=None, reqs=None, **kw):
        """
        Uses the Execution API to execute a dispel4py workflow

        Args
            | impl_id (int): the ID of the PEImpl to be executed
            | pckg (str): the name of the PEImpl's package
            | workspace_id (int): the ID of the relevant workspace
            | pe_name (str): the name of the PE
            | n_nodes (int): the number of the containers to be created
            | image (str): the tag of the image of the container. If it's not provided the latest tag is used. The base
            tag
            is the registry.gitlab.com/project-dare/dare-platform/exec-context-d4p and we add the tag at the end, e.g.
            registry.gitlab.com/project-dare/dare-platform/exec-context-d4p:latest
            | reqs (str): URL to a requirements txt file for the execution

        Returns
            | tuple: the response status code and text
        """
        # Prepare data for posting
        data = {
            "impl_id": impl_id,
            "pckg": pckg,
            "wrkspce_id": workspace_id,
            "name": pe_name,
            "n_nodes": n_nodes,
            "access_token": self.token,
            "reqs": reqs if not (reqs is None) else "None"
        }
        if image:
            data["image"] = image
        d4p_args = {}
        for k in kw:
            d4p_args[k] = kw.get(k)
        data['d4p_args'] = json.dumps(d4p_args)
        # Request for dare api
        _r = requests.post(self.EXEC_API_URL + '/run-d4p', data=data)
        # Progress check
        if _r.status_code != 200:
            print('DARE api resource / d4p-mpi-spec returns status_code: \
                    ' + str(_r.status_code))
            print(_r.text)
            return
        return _r.status_code, _r.text

    def debug_d4p(self, impl_id, workspace_id, pe_name, reqs=None, output_filename="output.txt",
                  pckg="main", file_format="txt", **kw):
        """
        Function to test a dispel4py workflow in the playground
        """
        # Prepare data for posting
        data = {
            "impl_id": impl_id,
            "pckg": pckg,
            "wrkspce_id": workspace_id,
            "n_nodes": 1,
            "name": pe_name,
            "access_token": self.token,
            "output_filename": output_filename,
            "output_file_format": file_format,
            "reqs": reqs if not (reqs is None) else "None"
        }
        d4p_args = {}
        for k in kw:
            d4p_args[k] = kw.get(k)
        data['d4p_args'] = json.dumps(d4p_args)
        r = requests.post(self.EXEC_API_URL + '/playground', data=data)
        if r.status_code == 200:
            response = json.loads(r.text)
            if response["logs"]:
                print("Logs:\n========================")
                for log in response["logs"]:
                    print(log)
            if response["output"]:
                if file_format == "txt":
                    print("Output content:\n==============================")
                    for output in response["output"]:
                        print(output)
                elif file_format == "json":
                    print(response["output"])
        else:
            print('Playground returns status_code: \
                    ' + str(r.status_code))
            print(r.text)

    def submit_cwl(self, workflow_name, workflow_version, input_data=None, nodes=None, save_workflow=None):
        """
            Uses the Execution API to execute a CWL workflow

            Args
                | workflow_name (str): the name of the registered workflow to be executed
                | workflow_version (str): the version of the workflow to be executed
                | input_data (dict): a dictionary with the necessary input data
                | nodes (int): the number of the containers to be created
            Returns
                | tuple: the response status code and text
            """
        url = self.EXEC_API_URL + "/run-cwl"

        data = {
            "access_token": self.token,
            "workflow_name": workflow_name,
            "workflow_version": workflow_version,
            "save_workflow": save_workflow
        }

        if input_data:
            data["input_data"] = json.dumps(input_data)
        if nodes:
            data["nodes"] = nodes

        response = requests.post(url, data=data)
        return response.status_code, response.text

    # *************************** Monitor Executions **********************************
    def my_pods(self):
        """
        Lists all the running containers for a specific user based on his/her token

        Returns
            json: the response of the Execution API for the running containers
        """
        _r = requests.get(self.EXEC_API_URL + '/my-pods?access_token=' + self.token)
        return _r.text

    @staticmethod
    def pod_pretty_print(_json):
        """
        Uses the response from function mypods to print them in a more user-friendly way
        """
        print('Running containers...')
        print('\n')
        for i in _json:
            print('Container name: ' + i['name'])
            print('Container status: ' + i['status'])
            print('\n')
        print('\n')

    def monitor(self):
        """
        Monitor function that uses the mypods function to retrieve the running containers and the pod_pretty_print
        function to print them to the user.
        """
        while True:
            clear_output(wait=True)
            resp = self.my_pods()
            self.pod_pretty_print(json.loads(resp))
            if not json.loads(resp):
                break
            time.sleep(1)

    # *********************************** Files ***********************************
    def upload(self, remote_path, local_path):
        """
        Uses the Execution API to upload a zip file to the DARE platform

        Args
            | remote_path (str): the directory inside the uploads dir where the file should be stored
            | local_path (str): the local path where the zip file to be uploaded exists

        Returns
            json: the response of the Execution API
        """
        params = (
            ('dataset_name', 'N/A'),
            ('access_token', self.token),
            ('path', remote_path),
        )
        files = {
            'file': (local_path, open(local_path, 'rb')),
        }
        _r = requests.post(self.EXEC_API_URL + '/upload', params=params, files=files)
        return _r.text

    def list_folders(self, num_run_dirs=None):
        """
        Uses the Execution API to list the directories of a user

        Args
            num_run_dirs (int): the number of the directories to be listed

        Returns
            json: the response of the API
        """
        _r = requests.get("{}/my-files?access_token={}&num_run_dirs={}".format(self.EXEC_API_URL, self.token,
                                                                               num_run_dirs)) if num_run_dirs else \
            requests.get("{}/my-files?access_token={}".format(self.EXEC_API_URL, self.token))
        return _r.text

    def list_folder_files(self, remote_path):
        """
        Function to list the files inside a specific directory (select one of the listed dirs from the list_folders
        function

        Args
            | remote_path (str): the path to the requested folder

        Returns
            json: the API response
        """
        _r = requests.get(self.EXEC_API_URL + '/list?path=' + remote_path + "&access_token=" + self.token)
        return _r.text

    def download(self, remote_path, local_path):
        """
        Uses the Execution API to download a file from the platform

        Args
            | remote_path (str): the path where the file is in the platform
            | local_path (str): the local path to store the downloaded file
        """
        url = self.EXEC_API_URL + '/download?access_token=' + self.token + '&path=' + remote_path
        wget.download(url, out=local_path)
        return "File is downloaded to path: {}".format(local_path)

    def send2drop(self, path, kind, username=None, password=None, remote_dir=None):
        """
        Uses the execution API to send files in B2Drop

        Args
            | path (str): path to file to be uploaded

        Returns
            json: the API response
        """
        url = self.EXEC_API_URL + "/send2drop"
        data = {
            "path": path,
            "access_token": self.token,
            "upload_kind": kind
        }
        if username:
            data["b2drop_username"] = username,
        if password:
            data["b2drop_password"] = password,
        if remote_dir:
            data["remote_dir"] = remote_dir
        response = requests.post(url, data=data)
        return response.text

    @staticmethod
    def folders_pretty_print(_json, num_run_dirs=None):
        """
        Get the response from the list_folders function and prints the directories of a user in a more user-friendly
        format
        """
        print('Uploaded files......')
        print('\n')
        for i in _json['uploads']:
            print('Api Local path: ' + i['path'])
            print('\n')
        print('\n')
        print('Files generated from runs......')
        print('\n')
        if _json['run']:
            _json['run'] = sorted(_json['run'], key=lambda k: k['path'], reverse=True)
            if num_run_dirs and len(_json['run']) > num_run_dirs:
                _json['run'] = _json['run'][:num_run_dirs]
        for i in _json['run']:
            print('Api Local path: ' + i['path'])
            print('\n')
        print('\n')
        print("Files generated from trial runs.......")
        print('\n')
        for i in _json['debug']:
            print('Api Local path: ' + i['path'])
            print('\n')

    @staticmethod
    def files_pretty_print(_json):
        """
        Gets the response from the function list_folder_files and presents the result in a more user-friendly format
        """
        print('Listing files......')
        print('\n')
        for i in _json['files']:
            print('Api Local path: ' + i['path'].split('/')[-1])
            print('\n')
        print('\n')

    def get_file_content(self, filename, run_dir=None, out_dir=None, path=None):
        """
        Function to get the content of a JSON file in the platform.

        Args
            | filename (string): the name of the file to be loaded
            | run_dir (string): the name of the directory (e.g. user1_20201013_ghgfhllrtgrl)
            | out_dir (string): optional, if the name of the directory inside the run_dir is different than "output"
            | path (string): if run_dir and out_dir are not provided, it contains the full path to the folder that
            contains the file
        Returns
            | json response with the file content
        """
        url = self.EXEC_API_URL + "/file-content"
        params = {
            "access_token": self.token,
            "filename": filename
        }
        if run_dir:
            params["run_dir"] = run_dir
        if out_dir:
            params["output_dir"] = out_dir
        if path:
            params["path"] = path

        response = requests.get(url, params=params)
        return response.status_code, response.text

    def cleanup_dirs(self, uploads=False, runs=False, subfolder_runs=None, subfolder_uploads=None):
        """
        Uses the Execution API to clean up a user's directories (e.g. uploads, runs etc)

        Args
            | uploads (bool): True/False if the uploads should or not be cleaned
            | runs (bool): True/False if the runs folder should or not be cleaned
        """
        url = self.EXEC_API_URL + "/cleanup"
        headers = {"Content-Type": "application/json"}
        data = {
            "access_token": self.token
        }
        if uploads:
            data["uploads"] = "uploads"
        if runs:
            data["runs"] = "runs"
        if subfolder_runs:
            data["subfolder_runs"] = subfolder_runs
        if subfolder_runs:
            data["subfolder_uploads"] = subfolder_uploads
        response = requests.get(url, params=data, headers=headers)
        return response.status_code, response.text

    def stop_job(self, job_name, run_dir):
        """
        Function to stop a running job

        Args
            | job_name (string): the name of the running job
            | run_dir (string): the associated run directory

        Returns
            | tuple: the response status code and text
        """
        url = self.EXEC_API_URL + "/stop-job"
        headers = {"Content-Type": "application/json"}
        data = {
            "access_token": self.token,
            "job_name": job_name,
            "run_dir": run_dir
        }
        response = requests.get(url=url, params=data, headers=headers)
        return response.status_code, response.text
