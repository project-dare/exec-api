from os.path import join, exists
from os import getcwd, mkdir

import requests


def download_docker_files():
    docker_folder = join(getcwd(), "docker_files")
    if not exists(docker_folder):
        mkdir(docker_folder)

    # get the latest docker files
    dockerfile = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-specfem3d/Dockerfile")
    with open(join(docker_folder, "Dockerfile"), "w") as f:
        f.write(dockerfile.text)

    entrypoint = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-specfem3d/entrypoint.sh")
    with open(join(docker_folder, "entrypoint.sh"), "w") as f:
        f.write(entrypoint.text)

    cwl_files_script = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-specfem3d/exec_specfem.sh")
    with open(join(docker_folder, "exec_specfem.sh"), "w") as f:
        f.write(cwl_files_script.text)

    cwl_files_script = requests.get(
        "https://gitlab.com/project-dare/dare-platform/-/raw/master/containers/exec-context-specfem3d/run_simulations.py")
    with open(join(docker_folder, "run_simulations.py"), "w") as f:
        f.write(cwl_files_script.text)


def download_cwl_files():
    cwl_folder = join(getcwd(), "cwl_files")
    if not exists(cwl_folder):
        mkdir(cwl_folder)

    # get the workflow
    workflow = requests.get(
        "https://gitlab.com/project-dare/WP6_EPOS/-/raw/RA_total_script/specfem3d/specfem3d_test_input_cwl/run_test.cwl")
    with open(join(cwl_folder, "run_test.cwl"), "w") as f:
        f.write(workflow.text)

    # get the spec file
    spec = requests.get(
        "https://gitlab.com/project-dare/WP6_EPOS/-/raw/RA_total_script/specfem3d/specfem3d_test_input_cwl/run_test.yml")
    with open(join(cwl_folder, "run_test.yml"), "w") as f:
        f.write(spec.text)
